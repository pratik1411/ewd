<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
    	<?php echo get_template_part("menu"); ?>
    	<div class="banner-text-inner">
          <?php
			$my_id = 17;
			$post_id_5369 = get_post($my_id);
			$content = $post_id_5369->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]>', $content);
			echo $content;
         ?>
    	</div>
	</div>
</section>
	<section class="our-works-wrap">
	<div class="our-works">
   	  <div class="work">
	  <div id="container">
  <div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
	<ul class="our-team-nav">
	   <li class="active"><a href="<?php echo get_site_url(); ?>/blog/" class="active">Blog</a></li>
      <!-- <li><a href="<?php echo get_site_url(); ?>/etraffic-tv" >eTraffic TV</a></li> -->
	</ul>
  </div>
  <div class="blog-search">
	  	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<input type="hidden" name="post_type" value="post"/>
	        <input type="text" value="" name="s" id="s" placeholder="Search Blog Posts"/>
	        <input type="submit" id="searchsubmit" value=" " />
		</form>
	</div>
      <div class="c"></div>
  </div>
</div>
 <div class="blog-new">
  	<div class="blog-new-left">
    	<div class="blog-new-list">
        	<ul>
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			</ul>
        </div>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
        <div class="blog-new-psginastion-wrap">
        	<div class="blog-new-psginastion">
		 		<?php wp_pagenavi(); ?>
		 	</div>
    	</div>
    </div>
      	    
<?php echo get_template_part("join_community"); ?>

<section class="from-our-journal-wrap">
<?php echo get_template_part("social_network");
	echo get_template_part("other_link");
	echo get_template_part("work_center");?>
<?php get_footer(); ?>
