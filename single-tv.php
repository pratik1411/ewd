<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
    	<?php echo get_template_part("menu"); ?>
    	<div class="banner-text-inner">
          <?php
      			$my_id = 2040;
      			$post_id_5369 = get_post($my_id);
      			$content = $post_id_5369->post_content;
      			$content = apply_filters('the_content', $content);
      			$content = str_replace(']]>', ']]>', $content);
      			echo $content;
          ?>
    	</div>
	</div>
</section>
<section class="our-works-wrap">
  <div class="our-works">
      <div class="work">
    <div id="container">
  <div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
    <ul class="our-team-nav">
     <li><a href="<?php echo get_site_url(); ?>/journal">Blog</a></li>
     <li class="active"><a href="<?php echo get_site_url(); ?>/etraffic-tv" class="active" >eTraffic TV</a></li>
    </ul>
  </div>
  <div class="blog-search">
      <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
        <input type="hidden" name="post_type" value="tv" />
        <input type="text" value="" name="s" id="s" placeholder="Search videos..."/>
        <input type="submit" id="searchsubmit" value=" " />
    </form>
  </div>
      <div class="c"></div>
  </div>
</div>
  <div class="blog-new">
    <div class="blog-new-left">
      <div class="blog-new-list-detail" id="etraffic-tv-detail">
              <div>
                    <div class="etraffic-tv-detail-title">
                      <h3><?php the_title(); ?></h3>
                      <div class="etraffic-tv-author-date"><div class="tv-detail-comment"><?php comments_number("0","1","%"); ?> comments </div>by <?php the_author(); ?>: <span><?php the_time("F d, Y"); ?></span></div>
                    </div>
                    <div class="c"></div>
                </div>
          <div class="blog-detail-disc ">
        <iframe width="100%" src="<?php echo get_field('videourl'); ?>" frameborder="0" allowfullscreen></iframe>
        <?php while(have_posts()):the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
        
        </div>

        <div class="blog-new-comment">
          <?php comments_template(); ?>          
        </div>
      </div>
      </div>
    </div>
<div class="blog-new-right">
  <div class="blog-new-free-trial">
    <h4>Free Website Health Check </h4>
    We offer a free 20 page anaylsis of your website which indicates areas that need tweeking for a positive user experienceand Search Rankings.
  <div class="blog-new-free-trial-btn"><a href="#popup" class="up">Get instant report created</a></div>
</div>

<div class="website-health-check" id="popup">
		<div class="website-health-check-title">Free Website Health Check
	    		<div class="website-health-check-close"><a href="javascript:;"></a></div>
		</div>
	    <?php echo do_shortcode('[contact-form-7 id="2212" title="popup"]'); ?>
	</div>

<div class="blog-new-right-box">
    <h4>Join the etraffic community</h4>
    <div class="blog-new-join-etraffic-community">
        Connect with us on the following social media platforms.
        <div class="blog-new-join-etraffic-community-icon">
            <a href="http://www.etrafficwebdesign.com.au/feed"></a>
            <a href="<?php echo get_option('fbid'); ?>"></a>
            <a href="<?php echo get_option('twitterid'); ?>"></a>
            <a href="<?php echo get_option('gplus'); ?>"></a>
            <a href="<?php echo get_option('linkedid'); ?>"></a>
            <a href="<?php echo get_option('youtubeid'); ?>"></a>
        </div>
    </div>
</div>

<div class="blog-new-right-box">
      <h4>Categories</h4>
        <div class="blog-new-right-categories">
      <?php
        $taxonomy = 'tvcategory';
        $term_args=array(
          'hide_empty' => false,
          'orderby' => 'name',
          'order' => 'ASC'
        );
        $tax_terms = get_terms($taxonomy,$term_args);
        ?>
        <ul>
        <?php
        foreach ($tax_terms as $tax_term) {
        echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all %s " ), $tax_term->name ) . '" ' . '>' . $tax_term->name . '<span class="count">(' . $tax_term->count . ')</span></a></li>';
        }
        ?>
      </ul>
        </div>
    </div>

<div class="blog-new-right-box">
      <h4>Posts</h4>
        <div>
          <div class="recent-post" id="tabs">
          <ul class="blog-tabs">
            <li><a href="#tabs-1">Recent</a></li>
            <li><a href="#tabs-2">Popular</a></li>
              <div class="c"></div>
          </ul>
            
            <div class="c"></div>
            <div class="blog-panes">
              <div id="tabs-1">
              <?php query_posts("post_type=tv&orderby=date&order=Desc&posts_per_page=6"); ?>
                  <ul>
                  <?php while ( have_posts() ) : the_post(); ?>
                      <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                    </ul>
                </div>
                
              <div id="tabs-2">
              <?php query_posts("post_type=tv&orderby=comment_count&order=Desc&posts_per_page=6"); ?>
                  <ul>
                  <?php while ( have_posts() ) : the_post(); ?>
                      <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
        </div>
    </div>
    
<!-- <div class="blog-new-ad">
  <ul>
      <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
      <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
    </ul>
        <div class="c"></div>
  <ul>
      <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
      <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
    </ul>
</div> -->

</div>    
    <div class="c"></div>
  </div>
  </div>
      </div>
  </div>
</section>
<script type="text/javascript">
    $(window).ready(function(){
        $(".website-health-check-close").click(function(){
            $.fancybox.close();
        });
    });
</script>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
