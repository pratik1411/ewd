<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta charset="utf-8">
	<link rel="shortcut icon" href="http://www.etrafficwebdesign.com.au/wp-content/themes/etrafficwebdesign/images/favicon.png" type="image/x-icon"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery-ui-1.7.2.custom.css">
	<link href='http://fonts.googleapis.com/css?family=Merriweather:400,400italic,700' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.touchwipe.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.carousel.js"></script>
    
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-160x160.png" sizes="160x160" />
	<meta name="msapplication-TileColor" content="#da532c" />
	<meta name="msapplication-TileImage" content="/mstile-144x144.png" />

    <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/lightbox-2.6.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/screen.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/lightbox.css"> -->

<script>
     $(document).ready(function () {
        $('.header-wrap').height($(window).height());});
		$(window).resize(function() {
		$('.header-wrap').css('height', window.innerHeight+'px');
});
</script>


	
	<script type="text/javascript">
      $(function() {
		//vars
		var conveyor = $(".content-conveyor", $("#sliderContent")),
		item = $(".item", $("#sliderContent"));
		//set length of conveyor
		conveyor.css("width", item.length * parseInt(item.css("width")));
        //config
        var sliderOpts = {
		  max: (item.length * parseInt(item.css("width"))) - parseInt($(".viewer", $("#sliderContent")).css("width")),
          slide: function(e, ui) { 
            conveyor.css("left", "-" + ui.value + "px");
          }
        };
        //create slider
        $("#slider").slider(sliderOpts);

        $(".nav-wrap .menu-menu-1-container ul li:nth-child(3)").addClass("what-we-do-nav");

        $(".reason-to-choose-us div:first").appendTo(".reason-to-choose-us h2.reason-to-choose-us-title");

        $(".captchatext").appendTo(".contact-captcha");
		$(".contact-submit").appendTo(".contact-captcha");

      });
    </script>

<script>
$(document).ready(function () {
 $('#allinone_carousel_sweet').bind('orientationchange');
 });
 </script>
    <script>
$(document).ready(function () {
      $('.etraffic-headquaters-slide').cycle({
      });
	   $('.portfolio-slide').on('cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
		   console.log('after');
		   $('.portfolio-slide div.op').removeClass('op');			   
		   $(incomingSlideEl).next().addClass('op');
	   });	   
//		   $('.portfolio-slide div.op').removeClass('op');
//      $('.portfolio-slide').cycle({
	//	  });
		  
      $('.testi-slide').cycle({
      });

});
</script>

<SCRIPT LANGUAGE="JavaScript">
function PopUp(URL) {
var iMyWidth;
var iMyHeight;
//half the screen width minus half the new window width (plus 5 pixel borders).
iMyWidth = (window.screen.width/2) - (75 + 10);
//half the screen height minus half the new window height (plus title and status bars).
iMyHeight = (window.screen.height/2) - (100 + 50);
//Open the window.
var win2 = window.open(URL, 'Window2', "status=no,height=350,width=500,resizable=yes,left=" + iMyWidth + ",top=" + iMyHeight + ",screenX=" + iMyWidth + ",screenY=" + iMyHeight + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
win2.focus();
}
</SCRIPT>

<script type="text/javascript">
      $(function() {
		//vars
		var conveyor = $(".content-conveyor", $("#sliderContent1")),
		item = $(".item", $("#sliderContent1"));
		//set length of conveyor
		conveyor.css("width", item.length * parseInt(item.css("width")));
        //config
        var sliderOpts = {
		  max: (item.length * parseInt(item.css("width"))) - parseInt($(".viewer", $("#sliderContent1")).css("width")),
          slide: function(e, ui) { 
            conveyor.css("left", "-" + ui.value + "px");
          }
        };
        //create slider
        $("#slider1").slider(sliderOpts);
      });
    </script>
<script type="text/javascript">
var animation_started = false;
$(document).ready(function(){
	
	$(window).scroll(function() {
		if($(window).scrollTop() > 400 && !animation_started) {
			animation_started = true;
		   $( ".process-chart-round1" ).delay( 500 ).fadeIn( 400 );
		   $( ".process-chart-round2" ).delay( 1000 ).fadeIn( 400 );
		   $( ".process-chart-round3" ).delay( 1500 ).fadeIn( 400 );
		   $( ".process-chart-round4" ).delay( 2000 ).fadeIn( 400 );
		   $( ".process-chart-round5" ).delay( 2500 ).fadeIn( 400 );
		   $( ".process-icon1" ).delay( 3000 ).fadeIn( 400 );
		   $( ".process-icon2" ).delay( 3500 ).fadeIn( 400 );
		   $( ".process-icon3" ).delay( 4000 ).fadeIn( 400 );
		   $( ".process-icon4" ).delay( 4500 ).fadeIn( 400 );
		   $( ".process-icon5" ).delay( 5000 ).fadeIn( 400 );
		   $( ".process-icon6" ).delay( 5500 ).fadeIn( 400 );
		   $( ".process-icon7" ).delay( 6000 ).fadeIn( 400 );
		   $( ".process-icon8" ).delay( 6500 ).fadeIn( 400 );
		   $( ".process-icon9" ).delay( 7000 ).fadeIn( 400 );
		   $( ".process-icon10" ).delay( 7500 ).fadeIn( 400 );
		   $( ".process-icon11" ).delay( 8000 ).fadeIn( 400 );
		   $( ".process-icon12" ).delay( 8500 ).fadeIn( 400 );
		   $( ".process-icon13" ).delay( 9000 ).fadeIn( 400 );
		   $( ".process-icon14" ).delay( 9500 ).fadeIn( 400 );
		   $( ".process-icon15" ).delay( 10000 ).fadeIn( 400 );
		   $( ".process-icon16" ).delay( 10500 ).fadeIn( 400 );
		   $( ".process-icon17" ).delay( 11000 ).fadeIn( 400 );
		   $( ".process-icon18" ).delay( 11500 ).fadeIn( 400 );
		   $( ".process-icon19" ).delay( 12000 ).fadeIn( 400 );
		   $( ".process-icon19" ).delay( 12500 ).fadeIn( 400 );
		   $( ".process-icon20" ).delay( 13000 ).fadeIn( 400 );	
		}
	});
	
function blink(selector){
$(selector).fadeOut('', function(){
    $(this).fadeIn('', function(){
    blink(this);
    });
});
}
blink('.blink');
      });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $(".process-tab a, .our-process-chart a").click(function () {
		var id = $(this).attr('id'); 
		$('.process-tab-disc > div').hide();		
		$("." + $(this).attr('id')).fadeIn("slow");
		$('.process-tab a, .our-process-chart a').removeClass('active');
		
		var cls = $('.process-tab-disc > div:visible').attr('class').split(' ');
		
		for(var i in cls) {
				$('.our-process-chart a#'+cls[i]).addClass('active');	
				$('.process-tab a#'+cls[i]).addClass('active');
		}
		
    });
	 $(".process-tab a").first().click();
	 $(".process-tab a").first().addClass('active');
	 $(".our-process-chart a").first().addClass('active');
	});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $(".other-work-centers ul li a").click(function () {
		$(".other-work-centers ul li a").removeClass("active");
		$(this).addClass("active");
		$('.other-work-centers-address div').hide();
		$("." + $(this).attr('id')).fadeIn("slow");
    });});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(".nav-responsive a").click(function () {
		$(".nav-responsive a").removeClass("active");
		$(this).addClass( "active" );
		$('.nav_small').hide();
		$("." + $(this).attr('id')).fadeIn("slow");
    });});
</script>

<script type="text/javascript">
	$(document).ready(function(){
    $(".nav-m a.nav-icon").click(function () {		
		var current_li = $(this).parent();
		$(".nav-m > div").each(function(i,el) {			
			if($(el).parent().is(current_li)) {				
				$(el).prev().toggleClass("plus");
				$(el).slideToggle();				
			} else{
				$(el).prev().removeClass("plus");
				$(el).slideUp();
			}
		});
    });
	$('.nav-m > div').hide();
	});
</script>


	<script type="text/javascript">
	$(document).ready(function(){
		$('a[href^="#scroll"]').on('click',function (e) {
		    e.preventDefault();
		    var target = this.hash,
		    $target = $(target);
		    $('html, body').stop().animate({
		        'scrollTop': $target.offset().top
		    }, 2000, 'swing', function () {
		        window.location.hash = target;
		    });
		});
	});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-30831897-1']);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

	
	<link href="<?php echo get_template_directory_uri(); ?>/js/allinone_carousel.css" rel="stylesheet" type="text/css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>


    
<!--<script>$('.sliderContent').draggable();</script>-->
    
<script type="text/javascript">
$(function() {
        $(".work-detail-banner > img").each(function(i, elem) {
          var img = $(elem);
		  $(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });
        });
    });
</script>
    
    
<script type="text/javascript">
$(function() {
        $(".services-detail-tabs1 > img").each(function(i, elem) {
          var img = $(elem);
		  $(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>

<script type="text/javascript">
$(function() {
        $(".services-detail-tabs2 > img").each(function(i, elem) {
          var img = $(elem);
		  $(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>

<script type="text/javascript">
$(function() {
        $(".services-detail-tabs3 > img").each(function(i, elem) {
          var img = $(elem);
		  $(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(".services-detail-tabs-nav ul li a").click(function () {
		$(".services-detail-tabs-nav ul li a").removeClass("active");
		$(this).addClass( "active" );
		$('.services-detail-content > div').hide();
		$("." + $(this).attr('id')).fadeIn("slow");
    });
	$(".services-detail-tabs-prev, .services-detail-tabs-next").click(function() {
		var next;
		var par = $(".services-detail-tabs-nav ul li a.active").parent();
		if($(this).is(".services-detail-tabs-next"))
			next = par.next().size()>0 ? par.next() :  $(".services-detail-tabs-nav ul li").first();
		else
			next = par.prev().size()>0 ? par.prev() :  $(".services-detail-tabs-nav ul li").last();
		next.find('a').triggerHandler('click');
	});
});
</script>


<!--For Multi select Start -->
	
<script type="text/javascript">
	function DropDown(el) {
		this.dd = el;
		this.opts = this.dd.find('ul.dropdown > li');
		this.val = [];
		this.index = [];
		this.initEvents();
	}
	DropDown.prototype = {
		initEvents : function() {
			var obj = this;

			obj.dd.on('click', function(event){
				$(this).toggleClass('active');
				event.stopPropagation();
			});

			obj.opts.children('label').on('click',function(event){
				var opt = $(this).parent(),
					chbox = opt.children('input'),
					val = chbox.val(),
					idx = opt.index();

				($.inArray(val, obj.val) !== -1) ? obj.val.splice( $.inArray(val, obj.val), 1 ) : obj.val.push( val );
				($.inArray(idx, obj.index) !== -1) ? obj.index.splice( $.inArray(idx, obj.index), 1 ) : obj.index.push( idx );
			});
		},
		getValue : function() {
			return this.val;
		},
		getIndex : function() {
			return this.index;
		}
	}

	$(function() {

		var dd = new DropDown( $('.dd') );

		$(document).click(function() {
			// all dropdowns
			$('.wrapper-dropdown-4').removeClass('active');
		});

	});
</script>

<script type="text/javascript">
$(document).ready(function () {
    $(".multi-select select").change(function () {
        var str = "";
        str = $(this).find(":selected").text();
        $(this).next(".out").text(str);
    }).trigger('change');
})
</script>
    
<!--For Multi select Close -->

<script type="text/javascript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fancybox/jquery.fancybox-1.3.4.css" media="screen" />



<script type="text/javascript">
    $(document).ready(function() {
	$(".single").fancybox({
        });

	$(".up").fancybox({ 
		'showCloseButton': false,
		'scrolling': true,
		'margin':0,
		'padding':0,
	});

	$(".showreel-popup").fancybox({ 
		'showCloseButton': false,
		'scrolling': true,
		'margin':0,
		'padding':0,
		'onStart': function() { Wistia.embed("3op0vpb5fz"); }
	});

	$(".clientsay-popup").fancybox({ 
		'showCloseButton': false,
		'scrolling': true,
		'margin':0,
		'padding':0
	});

	$(".tv-popup").fancybox({ 
		'showCloseButton': false,
		'scrolling': true,
		'margin':0,
		'padding':0
	});
		
        $("a[rel=gallery]").fancybox({
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'titlePosition' 	: 'over',
            'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });

        $(".various").fancybox({
            'titlePosition'		: 'inside',
            'transitionIn'		: 'none',
            'transitionOut'		: 'none'
        });
    });
</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/timeline.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/timeline.css" />
<script type="text/javascript">
$(document).ready(function() {
	var getTimelineData = function() {
		var data = [];

		$('#timeline_data > div').each(function(i, el) {
			data.push({
					type:     'blog_post',
					date:     $(el).data('date'),
					title:    $(el).find('h2').text(),
					width:    477,
					content:  $(el).find('div').first().html()
				});
		});
		return data;
	};

	var timeline = new Timeline($('#timeline'), getTimelineData());
	timeline.setOptions({
		animation:   true,
		lightbox:    true,
		showYear:    true,
		allowDelete: false,
		columnMode:  'dual',
		order:       'desc'
	});
	timeline.display();
	
	$('#timeline .column_right .timeline_element:even').addClass("red").append( "<div class='arrow-dot'></div>" );
	$('#timeline .column_left .timeline_element:even').addClass("yellow").append( "<div class='arrow-dot'></div>" );
	$('#timeline .column_left .timeline_element:odd').addClass("black").append( "<div class='arrow-dot'></div>" );	
	$('#timeline .column_right .timeline_element:odd').addClass("green").append( "<div class='arrow-dot'></div>" );
	
	
	
	// menu click
	$(document).on('click', '#menu > div', function(e) {
		var year      = $(this).text();
		var scroll_to = year == 2013 ? '#timeline' : '#timeline_date_separator_' + year;

		$.scrollTo(scroll_to, 500);
	});

	// load more click
	var year = 2013;
	$('#loadmore').on('click', function(e) {
		var button = $(this);

		if (button.hasClass('loading')) {
			return;
		}

		year--;
		button.addClass('loading').text('Loading...');
		setTimeout(function() {
			var scroll_to = '#timeline_date_separator_' + year;

			button.removeClass('loading').text('Load More');
			$('<div>').text(year).appendTo($('#menu'));

			var new_data = getTimelineData([year]);
			timeline.appendData(new_data);
			$.scrollTo(scroll_to, 500);
		}, 1000);
	});
});
</script>
	
<script type="text/javascript">
    $(document).ready(function() {
	setInterval(function() { $('.blink0').animate({opacity:0.6},
	300, function() { $('.blink0').animate({opacity:1},300);  })  }, 1000);
      });
</script>
<script type="text/javascript">
    $(document).ready(function() {
	setInterval(function() { $('.banner-play-btn1').animate({opacity:0.0},
	300, function() { $('.banner-play-btn1').animate({opacity:1},300);  })  }, 500);
      });
</script>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.quicksand.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-animate-css-rotate-scale.js"></script>


<!--For New Blog Start -->

<script type="text/javascript">
	$(document).ready(function(){
	$( "#tabs" ).tabs();
	});
</script>

<script type="text/javascript">
$(document).ready(function(){ 
$(window).scroll(function(){
	if ($(this).scrollTop() > 300) {
		$('.top-arrow-tc').fadeIn();
	} else {
		$('.top-arrow-tc').fadeOut();
	}
}); 
$('.top-arrow-tc').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 600);
	return false;
});
});
</script>
<!--For New Blog Close -->

<script>
$(function() {
 setInterval(function() {
  var next = $('.our-client ul li.active').next().size()>0 ? $('.our-client ul li.active').next() : $('.our-client ul li').first();
  $('.our-client ul li').removeClass('active');
  $(next).addClass('active');
 }, 3000);
 $('.our-client ul li').first().addClass('active');

});
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$.validator.addMethod("NumbersOnly", function(value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");
	
    var validator = $("#cform").validate({
        errorElement: "p",
		rules: {
		captcha: {
		required: true
	},
	},
	messages: {
		captcha: "Captcha Code is required !",
},
}); });
 </script>

	<script src="<?php echo get_template_directory_uri(); ?>/js/allinone_carousel.js" type="text/javascript"></script>
<script>
 $('#allinone_carousel_sweet').bind('orientationchange');
 </script>

 <script type="text/javascript">
   var _mfq = _mfq || [];
   (function() {
       var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
       mf.src = "//cdn.mouseflow.com/projects/19a2694f-a071-4039-b909-8fa690e74ca0.js";
       document.getElementsByTagName("head")[0].appendChild(mf);
   })();
</script>

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

	
</head>

<body>
