<script>
 $('#allinone_carousel_sweet').bind('orientationchange');
 </script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/allinone_carousel.js" type="text/javascript"></script>

<div class="our-fresh-work">
	<h2>&#8226; FRESH OFF THE PRESS &#8226;</h2>
</div>
<div id="bannerBg">
  <div id="containingDiv">
    <div id="allinone_carousel_sweet">
      <div class="myloader"></div>
      <?php $i=a; ?>
      <?php query_posts("post_type=portfolio&order=desc&posts_per_page=15"); ?>
      <ul class="allinone_carousel_list">
        <?php while(have_posts()):the_post(); ?>
          <li data-title="<?= $i; ?>"><div class="123"><a href="<?php the_permalink(); ?>"><img src="<?php the_field('home_image'); ?>" /></a>
            <h5><?php the_title(); ?></h5><span><?php the_excerpt(); ?> </span>
            <div><div class="quick-insite"><a href="<?php the_field('siteurl'); ?>" rel="nofollow" target="_blank"></a></div><div class="view-detail"><a href="<?php the_permalink(); ?>"></a></div></div></div> 
            </li> 
          <?php $i++; ?>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
	<div class="bannerControls" style="opacity:1 !important; z-index:15987; visibility:visible !important;	display:block !important;">
		<div class="leftNav" style="opacity:1 !important;	visibility:visible !important;	display:block !important;"></div>
		<div class="rightNav"  style="opacity:1 !important;	visibility:visible !important;	display:block !important;"></div>
	</div>
    </div>
  </div>
</div>
  </div>
</div>
