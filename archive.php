<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
    	<?php echo get_template_part("menu"); ?>
    	<div class="banner-text-inner">
          <?php
      			$my_id = 2040;
      			$post_id_5369 = get_post($my_id);
      			$content = $post_id_5369->post_content;
      			$content = apply_filters('the_content', $content);
      			$content = str_replace(']]>', ']]>', $content);
      			echo $content;
          ?>
    	</div>
	</div>
</section>
<section class="our-works-wrap">
	<div class="our-works">
   	  <div class="work">
	  <div id="container">
  <div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
  	<ul class="our-team-nav">
     <li><a href="<?php echo get_site_url(); ?>/journal">Blog</a></li>
     <li class="active"><a href="<?php echo get_site_url(); ?>/etraffic-tv"  class="active" >eTraffic TV</a></li>
    </ul>
  </div>
<div class="blog-search">
	  	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	        <input type="hidden" name="post_type" value="tv" />
          <input type="text" value="" name="s" id="s" placeholder="Search videos..."/>
	        <input type="submit" id="searchsubmit" value=" " />
		</form>
	</div>
      <div class="c"></div>
  </div>
</div>
 <div class="etraffic-tv">
    	<div class="etraffic-tv-list">
			<h3 class="spage-title">

		<?php if ( have_posts() ) : ?>
			<?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'twentythirteen' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'twentythirteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentythirteen' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'twentythirteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentythirteen' ) ) );
					else :
						_e( 'Archives', 'twentythirteen' );
					endif;
				?></h3>
          <ul>
            <?php $i=1; ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<li>
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <div class="etraffic-tv-author-date">by <?php the_author(); ?>: <span><?php the_time('F d,Y'); ?></span></div>
                	<div class="comment-video">
                    	<div class="comment-video-arrow"></div>
                        <ul>
                        	<li><a href="<?php the_permalink(); ?>"><?php comments_number("0","1","%"); ?> comments </a></li>
                        	<li><a href="<?php the_permalink(); ?>"><span>Watch Video »</span></a></li>
                        </ul>
                        <div class="c"></div>
                    </div>
            </li>
                    <?php $i++; ?>
                <?php if($i>3) { ?>
                	</ul>
		            <div class="c"></div>
		            <ul>
                <?php } ?>
			<?php endwhile; ?>

			</ul><div class="c"></div>
		<?php wp_reset_query(); ?>
        <div class="blog-new-psginastion-wrap">
        	<div class="blog-new-psginastion">
		 		<?php wp_pagenavi(); ?>
		 	</div>
    	</div>

 
		<?php else : ?>
			<?php echo "<h1>Nothing found</h1>"; ?>
		<?php endif; ?>
<div class="c"></div>
  </div>
  </div>
   	  </div>
  </div>
</section>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
