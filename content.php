<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<li>
    <div class="blog-new-author">
        <div class="blog-new-author-photo"><?php the_author_image(); ?></div>
        <div class="blog-new-author-name"><?php the_author(); ?><span><?php the_time('M d, Y'); ?></span></div>
    </div>
                    
    <div class="blog-new-post">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <div class="blog-new-category">Category: <span><?php the_category(' '); ?></span></div>
        <div class="blog-new-disc"><?php echo get_excerpt('250'); ?></div>
        <div class="blog-new-read-more"><a href="<?php the_permalink(); ?>">Read more</a>
            <span>&nbsp;</span>   
            <a href="<?php the_permalink(); ?>"><?php comments_number('0','1','%'); ?> comments</a>
        </div>
    </div>
    <div class="c"></div>
</li>
