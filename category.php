<?php
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
    	<?php echo get_template_part("menu"); ?>
    	<div class="banner-text-inner">
          <?php
      			$my_id = 17;
      			$post_id_5369 = get_post($my_id);
      			$content = $post_id_5369->post_content;
      			$content = apply_filters('the_content', $content);
      			$content = str_replace(']]>', ']]>', $content);
      			echo $content;
          ?>
    	</div>
	</div>
</section>
<section class="our-works-wrap">
	<div class="our-works">
   	  <div class="work">
	  <div id="container">
	  	<div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
    <ul class="our-team-nav">
       <li class="active"><a href="<?php echo get_site_url(); ?>/blog" class="active">Blog</a></li>
      <!-- <li><a href="<?php echo get_site_url(); ?>/etraffic-tv" >eTraffic TV</a></li> -->
    </ul>
  </div>
  <div class="blog-search">
	  	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	        <input type="text" value="" name="s" id="s" placeholder="Search Blog Posts"/>
	        <input type="submit" id="searchsubmit" value=" " />
		</form>
	</div>
      <div class="c"></div>
  </div>
</div>
 <div class="blog-new">
  	<div class="blog-new-left">
    	<div class="blog-new-list">
        	<ul>
		<?php if ( have_posts() ) : ?>
			
				<h3 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h3>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			</ul>
        </div>
			<?php wp_reset_query(); ?>
        <div class="blog-new-psginastion-wrap">
        	<div class="blog-new-psginastion">
		 		<?php wp_pagenavi(); ?>
		 	</div>
    	</div>
    </div>
<div class="blog-new-right">
    	<div class="blog-new-free-trial">
        	<h4>Grab your 30 days free trial </h4>
            We offer a free 20 page anaylsis of your website which indicates areas that need tweeking for a positive user experienceand Search Rankings.
            <div class="blog-new-free-trial-btn"><a href="#popup" class="up">Get instant report created</a></div>
        </div>

	<div class="website-health-check" id="popup">
		<div class="website-health-check-title">Free Website Health Check
	    		<div class="website-health-check-close"><a href="javascript:;"></a></div>
		</div>
	    <?php echo do_shortcode('[contact-form-7 id="2212" title="popup"]'); ?>
	</div>
    
    <div class="blog-new-right-box">
    	<h4>etraffic TV</h4>
        <div class="blog-etraffic-tv">
            <a href="#tvpopup" class="tv-popup">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blog-etraffic-tv.jpg">
            </a>
        </div>
    </div>

    <div id="tvpopup" class="tvpopup">
    <div class="banner-video-close"><a href="#"></a></div>
    <div id="wistia_otyn0yh46y" class="wistia_embed"><div itemprop="video" itemscope itemtype="http://schema.org/VideoObject"><meta itemprop="name" content="eTraffic Group - Customer Testimonial Montage" /><meta itemprop="duration" content="PT1M45S" /><meta itemprop="thumbnailUrl" content="https://embed-ssl.wistia.com/deliveries/105574245b3c4d00e7aa150e61dd0e376c6…" /><meta itemprop="contentURL" content="https://embed-ssl.wistia.com/deliveries/266ae1322c1c03b696c670b0ac38cdf4db8…" /><meta itemprop="embedURL" content="
    https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04&banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin" /><meta itemprop="uploadDate" content="2013-12-16T12:53:40Z" /><object id="wistia_otyn0yh46y_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;position:relative;"><param name="movie" value="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04"><… name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="bgcolor" value="#000000"></param><param name="wmode" value="opaque"></param><param name="flashvars" value="banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin"></param><embed src="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin" name="wistia_otyn0yh46y_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed></object><noscript itemprop="description">eTraffic Group - Customer Testimonial Montage</noscript></div></div>
    <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
    <script>
    wistiaEmbed = Wistia.embed("otyn0yh46y");
    </script>
    <script charset="ISO-8859-1" src="//fast.wistia.com/embed/medias/otyn0yh46y/metadata.js"></script>
    </div>

<div class="blog-new-right-box">
    <h4>Join the etraffic community</h4>
    <div class="blog-new-join-etraffic-community">
        Connect with us on the following social media platforms.
        <div class="blog-new-join-etraffic-community-icon">
            <a href="http://www.etrafficwebdesign.com.au/feed"></a>
            <a href="<?php echo get_option('fbid'); ?>"></a>
            <a href="<?php echo get_option('twitterid'); ?>"></a>
            <a href="<?php echo get_option('gplus'); ?>"></a>
            <a href="<?php echo get_option('linkedid'); ?>"></a>
            <a href="<?php echo get_option('youtubeid'); ?>"></a>
        </div>
    </div>
</div>
<?php dynamic_sidebar("sidebar-4"); ?>
    <div class="blog-new-right-box">
    	<h4>Posts</h4>
        <div>
        	<div class="recent-post" id="tabs">
                    	<ul class="blog-tabs">
                        	<li><a href="#tabs-1">Recent</a></li>
                        	<li><a href="#tabs-2">Popular</a></li>
                            <div class="c"></div>
                        </ul>
                        
                        <div class="c"></div>
                        <div class="blog-panes">
                        	<div id="tabs-1">
                        	<?php query_posts("post_type=post&orderby=date&order=Desc&posts_per_page=6"); ?>
                            	<ul>
                            	<?php while ( have_posts() ) : the_post(); ?>
                                	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php endwhile; ?>
                                </ul>
                            </div>
                            
                        	<div id="tabs-2">
			                   	<?php query_posts("post_type=post&orderby=comment_count&order=Desc&posts_per_page=6"); ?>
                            	<ul>
                            	<?php while ( have_posts() ) : the_post(); ?>
                                	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php endwhile; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
    
<!-- <div class="blog-new-ad">
	<ul>
    	<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
    	<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
    </ul>
        <div class="c"></div>
	<ul>
    	<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
    	<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/blog-ad.jpg"></a></li>
    </ul>
    
</div> -->
</div> 

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
<div class="c"></div>
  </div>
  </div>
   	  </div>
  </div>
</section>
<script type="text/javascript">
    $(window).ready(function(){
        $(".website-health-check-close").click(function(){
            $.fancybox.close();
        });
    });
</script>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
