<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner team-banner" id="scroll">
  <div class="header-pattern">
    <div class="banner-text-inner">
      
	     <h1>Whooops!</h1>
<h2>What are<br />
you doing here?</h2>
<h3>It&rsquo;s looking like you may have taken a wrong turn.</h3>
     
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<div class="not-found-page"><h2>Don&rsquo;t worry... it happens to the best of us. </h2>
<h3><a href="http://www.etrafficwebdesign.com.au">Click here</a> for to go on home page</h3>
</div>

<?php echo get_template_part("footer_link"); ?>




<?php get_footer(); ?>
