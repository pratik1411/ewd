<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
  <div class="header-pattern">
    <div class="banner-text-inner">
    <?php the_field("slogun"); ?>
    </div>
    <?php echo get_template_part("menu"); ?>
	</div>
</section>

<section>
	<div class="service">
		<div class="services-detail-welcome-text">
			<h2><?php the_title(); ?></h2>
			<?php while(have_posts()):the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; wp_reset_query(); wp_reset_postdata();?>
		<div class="c"></div>
      	</div>
        <?php $connected = new WP_Query( array(
          'connected_type' => 'service_to_detail',
          'connected_items' => get_queried_object(),
          'nopaging' => true
        ) );
        if ( $connected->have_posts() ) :
    ?>
      	<div class="services-detail-tabs">
		<div class="services-detail-tabs-next"></div>
		<div class="services-detail-tabs-prev"></div>
		<div class="services-detail-tabs-nav">
    <?php $li=1; ?>
        <ul>
      <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
    			<li><a href="javascript:void(0)" id="services-detail-tabs<?php echo $li; ?>"><div class="what-we-do-icon"></div><span><?php echo $li; ?>. </span><?php //the_title(); ?><?php the_field('inner_title');?></a></li>
          <?php $li++; endwhile; wp_reset_postdata(); ?>
          </ul>
    	</div>
    	<div class="services-detail-content">
    
    <?php $connected1 = new WP_Query( array(
          'connected_type' => 'service_to_detail',
          'connected_items' => get_queried_object(),
          'nopaging' => true
        ) ); ?>
		<?php $i=1; ?>
		<?php while ( $connected1->have_posts() ) : $connected1->the_post(); ?>
    		<div class="services-detail-tabs<?php echo $i; ?>">
        	<img src="<?php echo get_template_directory_uri(); ?>/images/service-detail-1.jpg">
			<div class="services-content-tab-text">
   	    	  <div class="services-detail-content-left"><img src="<?php echo get_template_directory_uri(); ?>/images/service-detail-1-1.png"></div>
    	    	<div class="services-detail-content-right"><h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
                </div>
                <div class="c"></div>
	        </div>
        </div>
        <?php $i++; ?>
        <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
  <?php endif; ?>

    <?php echo get_template_part("what_we_do"); ?>
<div class="back-to-all-services">
	<div class="what-we-do-link"><a href="<?php echo get_site_url(); ?>/services/">Back to all Services</a></div>
</div>

	<?php 
        $value = get_post_meta('13','startproject',true);
        if( ! empty( $value ) ) 
          echo($value);
  	?>
</div>
</section>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>