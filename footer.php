<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<section class="footer-wrap">
<footer>
<div class="back-to-top"><a href="#scroll">Back to the top</a></div>
<div class="footer-nav"><?php wp_nav_menu(array('menu' => 'menu2',"after" =>"<span>|</span>")); ?></div>
<div class="copyright">© 2013 eTraffic Web Design</div>
</footer>
</section>
</section>

<div class="mobile-social">
	<ul>
    	<li><a href="http://www.facebook.com/eTrafficGroup" target="_blank" class="icon-facebook"></a></li>
    	<li><a href="http://twitter.com/eTrafficGroup" target="_blank" class="icon-twitter"></a></li>
    	<li><a href="http://www.youtube.com/user/eTrafficGroup" target="_blank" class="icon-youtube"></a></li>
        <li><a href="http://plus.google.com/111067911665788661802" target="_blank" class="icon-google-plus"></a></li>
        <li><a href="http://www.linkedin.com/company/2901955?trk=tyah" target="_blank" class="icon-linkedin"></a></li>
        <li><a href="http://pinterest.com/etrafficgroup/" target="_blank" class="icon-pinterest"></a></li>
        <li><a href="http://54.200.79.82/projects/ewd/wp-content/themes/ewd/images/yelp-icon.png" target="_blank" class="icon-yelp"></a></li>
    </ul>
</div>


<script>
		jQuery(function() {
			jQuery('#allinone_carousel_sweet').allinone_carousel({
				skin: 'sweet',
				width: jQuery(window).width(),
				height: 454,
				responsive:true,
				autoPlay: 3,
				resizeImages:true,
				autoHideBottomNav:false,
				//easing:'easeOutBounce',
				numberOfVisibleItems:3,
				elementsHorizontalSpacing:600,
				elementsVerticalSpacing:50,
				verticalAdjustment:0,
				animationTime:0.5,
				circleLeftPositionCorrection:50,
				circleTopPositionCorrection:20,
				nextPrevMarginTop:25,
				playMovieMarginTop:-15,
				bottomNavMarginBottom:-25
			});		
		});
	</script>
    
	<script>
		jQuery(function() {
			jQuery('#allinone_carousel_sweet1').allinone_carousel({
				skin: 'sweet',
				width: jQuery(window).width(),
				height: 525,
				responsive:true,
				autoPlay: 0,
				resizeImages:true,
				autoHideBottomNav:false,
				//easing:'easeOutBounce',
				numberOfVisibleItems:3,
				elementsHorizontalSpacing:960,
				elementsVerticalSpacing:50,
				verticalAdjustment:0,
				animationTime:0.5,
				circleLeftPositionCorrection:50,
				circleTopPositionCorrection:20,
				nextPrevMarginTop:25,
				playMovieMarginTop:-15,
				bottomNavMarginBottom:-25
			});		
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
		 $('a[href^="#our-process-tab"]').on('click',function (e) {
		     e.preventDefault();
		     var target = this.hash,
		     $target = $(target);
		     $('html, body').stop().animate({
		         'scrollTop': $target.offset().top
		     }, 2000, 'swing', function () {
		         window.location.hash = target;
		     });
		 });
		});
	</script>
	<script type="text/javascript">
$(document).ready(function(){
 $('a[href^="#webdesi"]').on('click',function (e) {
     e.preventDefault();
     var target = this.hash,
     $target = $(target);
     $('html, body').stop().animate({
         'scrollTop': $target.offset().top
     }, 2000, 'swing', function () {
         window.location.hash = target;
     });
 });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
 $('a[href^="#webdeve"]').on('click',function (e) {
     e.preventDefault();
     var target = this.hash,
     $target = $(target);
     $('html, body').stop().animate({
         'scrollTop': $target.offset().top
     }, 2000, 'swing', function () {
         window.location.hash = target;
     });
 });
});
</script>
<!-- begin olark code -->

<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){

f[z]=function(){

(a.s=a.s||[]).push(arguments)};var a=f[z]._={

},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){

f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={

0:+new Date};a.P=function(u){

a.p[u]=new Date-a.p[0]};function s(){

a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){

hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){

return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){

b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{

b.contentWindow[g].open()}catch(w){

c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{

var t=b.contentWindow[g];t.write(p());t.close()}catch(x){

b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({

loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});

/* custom configuration goes here (www.olark.com/documentation) */

olark.identify('9723-788-10-9050');/*]]>*/</script>
<noscript><a href="https://www.olark.com/site/9723-788-10-9050/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>

<!-- end olark code -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<?php wp_footer(); ?>
</body>
</html>
