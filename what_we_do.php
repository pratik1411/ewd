<section class="what-we-do-wrap">
  <div class="what-we-do">
    <h2>&#8226; What we do 	&#8226;</h2>
    <div>
    	<div class="">
          	  <ul class="etraffic-headquaters-slide"
        data-cycle-slides="> li"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="4"
        data-cycle-prev=".our-client-slider-prev"
        data-cycle-next=".our-client-slider-next">

                    <?php query_posts("post_type=service&posts_per_page=-1"); ?>
            <?php while(have_posts()):the_post(); ?>
       <li class="item">

          	<div class="what-we-do-content">
              <h3><?php the_title(); ?></h3>
          	<div class="what-we-do-text"><?php echo get_excerpt('250'); ?></div>
          	<div class="what-we-do-link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
              </div>
          </li>
          <?php endwhile; wp_reset_query(); ?>
          <div class="c"></div>
    </ul>
	</div>
          <div class="our-client-slider-arrow">
		<a href="#" class="our-client-slider-prev"></a>
        <a href="#" class="our-client-slider-next"></a>
    </div>
            </div>
      </div>
  </div>
</section>
