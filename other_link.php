<div class="other-social-links">
	<h4>&#8226; Other social links &#8226;</h4>
	<ul>
    	<li><a href="<?php echo get_option('fbid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook-icon.png"></a></li>
    	<li><a href="<?php echo get_option('youtubeid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/youtube-icon.png"></a></li>
    	<li><a href="<?php echo get_option('gplus'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/google-icon.png"></a></li>
    	<li><a href="<?php echo get_option('linkedid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/linked-icon.png" alt=""></a></li>
    	<li><a href="<?php echo get_option('pintrestid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/pintrest-icon.png"></a></li>
    	<li><a href="<?php echo get_option('yelpid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/yelp-icon.png"></a></li>
    	<li><a href="<?php echo get_option('twitterid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter-icon.png"></a></li>
	<li><a href="<?php echo get_option('partnerid'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/partner.png"></a></li>
    </ul>
</div>
