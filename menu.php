<div class="nav-wrap">
      <div class="wrap">
        <div class="logo">
          <a href="<?php echo get_site_url(); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
          </a>
        </div>
        <nav>
          <?php wp_nav_menu(); ?>
          <div class="c"></div>
        </nav>
        <div class="nav-responsive">
          <div class="nav-m">
            <a href="#" class="nav-icon"></a>
            <?php wp_nav_menu(array('menu'=>'Menu 1')); ?>
            <div class="c"></div>
          </div>
        </div>
      </div>
    </div>