<?php
/**
 * Template Name: Our Work Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
  <div class="header-pattern">
     <div class="banner-text-inner">
        <?php the_field("slogun"); ?>
     </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-works-wrap">
  <div class="our-works">
    <div class="work">
      <div id="container">
        <?php echo do_shortcode('[nimble-portfolio template="rect-1"]'); ?>
      </div>
    </div>
   <div class="works-testimonials">
<?php query_posts("post_type=testimonial&orderby=rand&posts_per_page=1"); ?>
<?php while(have_posts()):the_post(); ?>
  <?php the_content(); ?>
    <span><?php the_title(); ?></span>
  <?php endwhile; ?>
</div>
  </div>
</section>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>