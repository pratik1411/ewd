<?php
/**
 * Template Name: What we do Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
</div>
<div class="header-pattern">
  <div class="banner-text-inner">
    <?php while(have_posts()) : the_post();?>
      <?php the_content(); ?>
    <?php endwhile;  wp_reset_query();?>
  </div>
  <?php echo get_template_part("menu"); ?>
  </div>
</section>
<section>
  <div class="service">
    <?php the_field("service_category"); ?>
    <div class="webdesign-service-banner">
      <div class="webdesign-service-banner-title">Web<br>Design</div>
    </div>
    <div class="services-content">
	<p id="webdesi" tabindex="-1"></p>
      <?php the_field("design"); ?>
      <div class="c"></div>
    </div>
    <div class="services-design">
      <div class="services-design-slider">
        <div>
          <div>
            <div>
<?php query_posts('post_type=service&taxonomy=service-category&term=web-design'); ?>
<ul class="etraffic-headquaters-slide"
        data-cycle-slides="> li"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="2"
        data-cycle-prev=".our-client-slider-prev"
        data-cycle-next=".our-client-slider-next">
<?php while(have_posts()):the_post(); ?>
            <li class="item">
                <div class="service-design-slider-content">
                      <div class="services-design-left"><img src="<?php echo get_field('icon'); ?>"></div>
                      <div class="services-design-right">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php echo get_excerpt('350'); ?>
                  </div>
                    </div>
              </li>
            <?php endwhile; wp_reset_query(); ?>
              <div class="c"></div>
                </ul>
          </div>
	<div class="our-client-slider-arrow">
		<a href="#" class="our-client-slider-prev"></a>
        	<a href="#" class="our-client-slider-next"></a>
    	</div>
            </div>
      </div>
        </div>


    <div class="web-development-service-banner">
      <div class="webdesign-service-banner-title">Web<br>Development</div>
    </div>
    <div class="services-content">
	<p id="webdeve" tabindex="-1"></p>
      <?php the_field("development"); ?>
      <div class="c"></div>
    </div>

    <div class="services-design">
         <div class="services-design-slider">
        <div>
              <div>
            <div>
            <?php query_posts('post_type=service&taxonomy=service-category&term=web-development&posts_per_page=-1'); ?>
<ul class="etraffic-headquaters-slide"
        data-cycle-slides="> li"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="2"
        data-cycle-prev=".our-client-slider-prev"
        data-cycle-next=".our-client-slider-next">
			<?php while(have_posts()):the_post(); ?>
                  <li class="item">
                <div class="service-design-slider-content">
                      <div class="services-design-left"><img src="<?php echo get_field('icon'); ?>"></div>
                      <div class="services-design-right">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php echo get_excerpt('350'); ?>
                  </div>
                    </div>
              </li>
              <?php endwhile; wp_reset_query(); ?>
                  <div class="c"></div>
                </ul>
          </div>
              <div class="our-client-slider-arrow">
		<a href="#" class="our-client-slider-prev"></a>
        	<a href="#" class="our-client-slider-next"></a>
    	</div>
            </div>
      </div>
        </div>
    <?php the_field("startproject"); ?>
  </div>
</section>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
