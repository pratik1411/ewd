<?php
/**
 * Template Name: career Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner team-banner" id="scroll">
  <div class="header-pattern">
    <div class="banner-text-inner">
      <?php while(have_posts()) : the_post();?>
        <?php the_field('slogun'); ?>
      <?php endwhile;  wp_reset_query();?>
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-works-wrap">
  <div class="our-works">
    <div id="container">
      <div class="work-nav">
        <ul class="our-team-nav">
          <li><a href="<?php echo get_site_url(); ?>/about-us/" >About Us</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-team/"  >Our Team</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-history/" >Our History</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-process/">Our Process</a></li>
          <li><a href="<?php echo get_site_url(); ?>/career/" class="active">Careers</a></li>
            </ul>
        <div class="c"></div>
      </div>
      </div></div></section>

<section>
  <div class="service">
      
<div class="career-wrap">
	<div class="career">
    
<ul>
<?php $car = new WP_Query(array('post_type'=>'ewdcareer','order'=>'asc'));
	while($car->have_posts()) : $car->the_post();?>
<li>
	<h2><?php the_title();?></h2>
	<?php the_content();?>
	<?php the_field('how_to_apply');?>
</li>
    
<?php endwhile;?>
<?php wp_reset_query();?>
</ul>
<div class="c"></div>
    </div>
	</div>
</div></section>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
