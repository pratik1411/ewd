<?php
/**
 * Template Name: Our history Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner our-histry-banner" id="scroll">
  <div class="header-pattern">
     <div class="banner-text-inner">
        <?php the_field("slogun"); ?>
     </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-works-wrap">
<div class="work-nav">
  <ul class="our-team-nav">
   <li><a href="<?php echo get_site_url(); ?>/about-us/" >About Us</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-team/" >Our Team</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-history/" class="active" >Our History</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-process/"  >Our Process</a></li>
          <li><a href="<?php echo get_site_url(); ?>/career/">Careers</a></li>
      </ul>
      <div class="c"></div>
</div>

<div class="histry-subtext">
<h3>Under Construction</h3>

<?php while(have_posts()):the_post(); ?>
  <?php //the_content(); ?>
<?php endwhile; ?>
</div>
<!-- <div class="timeline-wrap">
  <div id="timeline"></div>
    <div id="timeline_data" style="display: none;">         
    <?php query_posts("post_type=timeline&posts_per_page=-1"); ?>
    <?php while(have_posts()):the_post(); ?>
   <div data-color="yellow" data-date="<?php the_time('Y-m-d'); ?>">
          <div>
          <h2><?php the_title() ?></h2>
            <?php the_content(); ?>
          </div>
      </div>
    <?php endwhile; wp_reset_query(); ?>

  </div>
            </div> -->
</section>

<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>