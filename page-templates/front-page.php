<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap" id="scroll">

<div class="header-pattern">
      <?php echo get_template_part("menu"); ?>
      <div class="banner-text">
      	<h1>Creating Premium Websites <br>At Affordable Prices </h1>
        Made By An Australian Business For Australian Businesses 
      </div>


      <div class="banner-video-bg">
      <div class="banner-new-showreel"><a href="#showreel" class="showreel-popup">
      <span>View Our </span>SHOWREEL</a></div>
</div>






  </div>
    </section>
<?php echo get_template_part("what_we_do"); ?>

<section class="reason-to-choose-us-wrap">
<div id="showreel" class="showreel">
<div class="banner-video-close"><a href="#"></a></div>

<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
<script>
//var wistiaEmbed = Wistia.embed("3op0vpb5fz");
</script>
<script charset="ISO-8859-1" src="//fast.wistia.com/embed/medias/3op0vpb5fz/metadata.js"></script>
<div id="wistia_3op0vpb5fz" class="wistia_embed"><div itemprop="video" itemscope itemtype="http://schema.org/VideoObject"><meta itemprop="name" content=""Website Portfolio" eTraffic Web Design "Website Design Portfolio"" /><meta itemprop="duration" content="PT1M24S" /><meta itemprop="thumbnailUrl" content="https://embed-ssl.wistia.com/deliveries/07a8bab6a80b1cf38ca88600605d1150627Â…" /><meta itemprop="contentURL" content="https://embed-ssl.wistia.com/deliveries/b668ac241bdf869f4c1a86c1eb7a6f63489Â…" /><meta itemprop="embedURL" content="
https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04&banner=true&controlsVisibleOnLoad=true&customColor=424139&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=27839743&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc6ee9d1efd4d5d2b465069c6ac27f2e7c7becfcf.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=84.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F07a8bab6a80b1cf38ca88600605d11506279a828.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fb668ac241bdf869f4c1a86c1eb7a6f6348908f04.bin" /><meta itemprop="uploadDate" content="2013-12-16T05:26:24Z" /><object id="wistia_3op0vpb5fz_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;height:360px;position:relative;width:640px;"><param name="movie" value="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04"><Â… name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="bgcolor" value="#000000"></param><param name="wmode" value="opaque"></param><param name="flashvars" value="banner=true&controlsVisibleOnLoad=true&customColor=424139&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=27839743&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc6ee9d1efd4d5d2b465069c6ac27f2e7c7becfcf.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=84.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F07a8bab6a80b1cf38ca88600605d11506279a828.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fb668ac241bdf869f4c1a86c1eb7a6f6348908f04.bin"></param><embed src="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="banner=true&controlsVisibleOnLoad=true&customColor=424139&endVideoBehavior=default&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=27839743&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc6ee9d1efd4d5d2b465069c6ac27f2e7c7becfcf.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=84.0&playButtonVisible=true&showPlayButton=true&showPlaybar=true&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F07a8bab6a80b1cf38ca88600605d11506279a828.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fb668ac241bdf869f4c1a86c1eb7a6f6348908f04.bin" name="wistia_3op0vpb5fz_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed></object><noscript itemprop="description">"Website Portfolio" eTraffic Web Design "Website Design Portfolio"</noscript></div></div>


</div>

<div class="reason-to-choose-us">
	<div class="our-fresh-work">
	      <h2>&#8226; What Our Clients Say &#8226;</h2>
	</div>
      <div class="reason-to-choose-us-sub-title">Click Play To Watch</div>
        <div class="reason-to-choose-us-video">
          <a href="#clientsay" class="clientsay-popup">
            <img src="<?php echo get_template_directory_uri(); ?>/images/index-video2.jpg">
          </a>
        </div>
    </div>

<div id="clientsay" class="clientsay">
<div class="banner-video-close"><a href="#"></a></div>
<div id="wistia_otyn0yh46y" class="wistia_embed"><div itemprop="video" itemscope itemtype="http://schema.org/VideoObject"><meta itemprop="name" content="eTraffic Group - Customer Testimonial Montage" /><meta itemprop="duration" content="PT1M45S" /><meta itemprop="thumbnailUrl" content="https://embed-ssl.wistia.com/deliveries/105574245b3c4d00e7aa150e61dd0e376c6…" /><meta itemprop="contentURL" content="https://embed-ssl.wistia.com/deliveries/266ae1322c1c03b696c670b0ac38cdf4db8…" /><meta itemprop="embedURL" content="
https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04&banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin" /><meta itemprop="uploadDate" content="2013-12-16T12:53:40Z" /><object id="wistia_otyn0yh46y_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;height:360px;position:relative;width:640px;"><param name="movie" value="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04"><a name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="bgcolor" value="#000000"></param><param name="wmode" value="opaque"></param><param name="flashvars" value="banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin"></param><embed src="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin" name="wistia_otyn0yh46y_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed></object><noscript itemprop="description">eTraffic Group - Customer Testimonial Montage</noscript></div></div>
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
<script>
wistiaEmbed = Wistia.embed("otyn0yh46y");
</script>
<script charset="ISO-8859-1" src="//fast.wistia.com/embed/medias/otyn0yh46y/metadata.js"></script>
</div>

	<?php echo get_template_part("fresh_work"); ?>    
</section>

<?php the_content(); ?>

<section class="our-client-wrap">
<div class="our-client">
  <h2>&#8226; Our Clients &#8226;</h2>
    <?php $i=1;?>
    <?php $xo = query_posts("post_type=client&posts_per_page=-1"); ?>
<ul class="etraffic-headquaters-slide"
        data-cycle-slides="> li"
        data-cycle-fx="carousel"
		data-cycle-carousel-fluid=true
        data-cycle-carousel-visible="4"
        data-cycle-prev=".our-client-slider-prev"
        data-cycle-next=".our-client-slider-next"
	    data-cycle-timeout=5000 > 

      <?php while(have_posts()):the_post(); ?>
        <?php if($i%2==0): ?>
      <li><a href="javascript:;"><?php the_post_thumbnail(); ?></a></li>
    <?php endif; ?>
    <?php $i++; ?>
	<?php endwhile;  wp_reset_query(); ?>
		</ul>
    <?php $j=1;?>
<?php $xo = query_posts("post_type=client&posts_per_page=-1"); ?>
<ul class="etraffic-headquaters-slide"
        data-cycle-slides="> li"
        data-cycle-fx="carousel"
		data-cycle-carousel-fluid=true
        data-cycle-carousel-visible="4"
        data-cycle-prev=".our-client-slider-prev"
        data-cycle-next=".our-client-slider-next"
      data-cycle-timeout=5000 >

      <?php while(have_posts()):the_post(); ?>
        <?php if($j%2!=0): ?>
    <li class="our-client-bottom"><a href="javascript:;"><?php the_post_thumbnail(); ?></a></li>
     <?php endif; ?>
    <?php $j++; ?>
  <?php endwhile; ?>
    </ul>
      
</div>
</div>
<div class="c"></div>
<div class="our-client-slider-arrow">
		<a href="#" class="our-client-slider-prev"></a>
        <a href="#" class="our-client-slider-next"></a>
    </div>

</div>
</section>
<script type="text/javascript">
    $(window).ready(function(){
        $(".banner-video-close").click(function(){
            $.fancybox.close();
        });
    });
</script>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>