<?php
/**
 * Template Name: Tv Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
    	<?php echo get_template_part("menu"); ?>
    	<div class="banner-text-inner">
          <?php
			$my_id = 2040;
			$post_id_5369 = get_post($my_id);
			$content = $post_id_5369->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]>', $content);
			echo $content;
         ?>
    	</div>
	</div>
</section>
	<section class="our-works-wrap">
	<div class="our-works">
   	  <div class="work">
	  <div id="container">
  <div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
    <ul class="our-team-nav">
     <li><a href="<?php echo get_site_url(); ?>/journal">Blog</a></li>
     <li class="active"><a href="<?php echo get_site_url(); ?>/etraffic-tv"  class="active" >eTraffic TV</a></li>
    </ul>
  </div>
  <div class="blog-search">
	  <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
      <input type="hidden" name="post_type" value="tv" />
      <input type="text" value="" name="s" id="s" placeholder="Search Videos..."/>
      <input type="submit" id="searchsubmit" value=" " />
		</form>
	</div>
      <div class="c"></div>
  </div>
</div>
 
 <div class="etraffic-tv">
    	<div class="etraffic-tv-list">
    	<?php 
      $paged = 1;
      if ( get_query_var('paged') ) $paged = get_query_var('paged');
      if ( get_query_var('page') ) $paged = get_query_var('page');
      $loop=new WP_Query(array('post_type'=>'tv','posts_per_page'=>15,'paged' => $paged, 'posts_per_page' => 9)); ?>
       	  <?php $i=1; ?>
       	  <ul>
       	  	<?php while($loop->have_posts()): $loop->the_post(); ?>
            	<li>
                	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <div class="etraffic-tv-author-date">by <?php the_author(); ?>: <span><?php the_time('F d,Y'); ?></span></div>
                	<div class="comment-video">
                    	<div class="comment-video-arrow"></div>
                        <ul>
                        	<li><a href="<?php the_permalink(); ?>"><?php comments_number("0","1","%"); ?> comments </a></li>
                        	<li><a href="<?php the_permalink(); ?>"><span>Watch Video »</span></a></li>
                        </ul>
                        <div class="c"></div>
                    </div>
                </li>
                <?php $i++; ?>
                <?php if($i>3) { ?>
                	</ul>
		            <div class="c"></div>
		            <ul>
                <?php } ?>
            <?php endwhile; ?>
                </ul><div class="c"></div>
<div class="blog-new-psginastion-wrap">
        	<div class="blog-new-psginastion">
            	<?php if (function_exists('wp_pagenavi')) {
        wp_pagenavi( array( 'query' => $loop ) );
      wp_reset_postdata();
    } ?>
            </div>
        </div>
        
    </div>
</div>
  </div>
</section>

<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
