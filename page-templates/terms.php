<?php
/**
 * Template Name: Terms Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
      <div class="header-pattern">
    <div class="banner-text-inner">
      <?php while(have_posts()) : the_post();?>
        <?php the_field('slogun'); ?>
      <?php endwhile;  wp_reset_query();?>
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>
<section>
  <div class="service">
      
<div class="career-wrap">
	<div class="terms">
	<?php while(have_posts()):the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>
<div class="top-arrow-tc"><a href="#"></a></div>
<div class="c"></div>
    </div>
	</div>
</section>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
