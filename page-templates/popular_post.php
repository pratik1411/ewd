<?php
/**
 * Template Name: Popular post Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
      <?php echo get_template_part("menu"); ?>
      <div class="banner-text-inner">
          <?php
      $my_id = 17;
      $post_id_5369 = get_post($my_id);
      $content = $post_id_5369->post_content;
      $content = apply_filters('the_content', $content);
      $content = str_replace(']]>', ']]>', $content);
      echo $content;
         ?>
      </div>
  </div>
</section>
  <section class="our-works-wrap">
  <div class="our-works">
      <div class="work">
    <div id="container">
  <div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
  <?php echo get_template_part("blog_navigation"); ?>
  </div>
  <?php dynamic_sidebar("sidebar-3"); ?>
      <div class="c"></div>
  </div>
  <div class="blog">
  <ul>
  <?php 
              $paged = 1;
              if ( get_query_var('paged') ) $paged = get_query_var('paged');
              if ( get_query_var('page') ) $paged = get_query_var('page');
        ?>
      <?php $loop1 = new WP_Query(array('post_type' => 'post','orderby' =>'comment_count' ,'order' => 'Desc' , 'posts_per_page' => 10, 'paged' => $paged ) );  ?>
    <?php if ( $loop1->have_posts() ) : ?>
      <?php while ( $loop1->have_posts() ) : $loop1->the_post(); ?>
        <?php get_template_part( 'content', get_post_format() ); ?>
      <?php endwhile; ?>
    <?php endif; ?>
    <?php wp_pagenavi( array( 'query' => $loop1 ) );
     wp_reset_postdata(); ?>
    </div> 
  </div>
<section class="from-our-journal-wrap">
<?php echo get_template_part("social_network");
  echo get_template_part("other_link");
  echo get_template_part("work_center");?>
<?php get_footer(); ?>