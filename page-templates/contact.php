<?php
/**
 * Template Name: Conatct Us Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

if(isset($_POST['submit']))
{

  if(strtoupper($_POST['captcha']) == $_SESSION['captcha_id'])
  {  
    @session_start();
            $url = "http://54.200.79.82/projects/ewd/wp-content/themes/ewd";
            ob_start();         
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>eTraffic Web Design</title>
      </head>
      <body topmargin="0" leftmargin="0" style="padding:0; margin:0; background-color:#f7f7f7;">
      <table cellpadding="0" cellspacing="0" align="center" width="100%" style="background-color:#f7f7f7; font-family:Arial, Helvetica, sans-serif">
          <tr>
              <td valign="top">
                  <table cellpadding="0" cellspacing="0" width="718" align="center" style="background:url(<?= $url ?>/images/top-bg.png); background-repeat:no-repeat; background-position:center top; margin-top:20px;">
                      <tr>
                          <td width="18"></td>
                          <td width="598" style="background-color:#ffffff; padding:42px" valign="top">
                              <table cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                      <td height="110" align="center" valign="top"><a href="#"><img src="<?= $url ?>/images/logo.png" alt="etraffic-logo" /></a></td>
                                  </tr>
                                  <tr>
                                      <td style="background-color:#4b565c; color:#ffffff; font-size:18px; text-transform:uppercase; font-family:Arial, Helvetica, sans-serif; font-weight:bold; padding:10px;">
                                      A new lead generated
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style="color:#000000; font-weight:bold; font-size:14px;">Hi Administrator</td>
                                 </tr>
                                  <tr>
                                      <td style="color:#000000; font-size:14px; padding:15px 0px;">Great News!</td>
                                  </tr>
                                  <tr>
                                      <td style="color:#000000; font-size:14px;">We have a new lead for you</td>
                                  </tr>
                                  <tr>
                                      <td>&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td valign="top" style="background-color:#f8f8f8; padding:5px">
                                          <table cellpadding="0" cellspacing="0" width="100%" style="background-color:#ffffff; color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif;">
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px">                        Name
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?= $_POST['fullname'] ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Email
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px">
                                                      <a href="#" style="color:#555555; text-decoration:none"><?= $_POST['email'] ?></a>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Phone
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?= @$_POST['phone'] ?>
                                                  </td>
                                              </tr>
                                              <tr>                                         <tr>
                                                <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                         State
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?= @$_POST['state'] ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Project Requirement
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?php foreach (@$_POST['project'] as $key => $value) {
                                                          if(empty($value))
                                                          {
                                                              unset($_POST['project'][$key]);
                                                          }
                                                      } ?>
                                                      <?= @implode(',',$_POST['project']) ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Website Type
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?php foreach (@$_POST['website'] as $key => $value) {
                                                          if(empty($value))
                                                          {
                                                              unset($_POST['website'][$key]);
                                                          }
                                                      } ?>
                                                      <?= @implode(',',$_POST['website']) ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Time Frame
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?= @$_POST['timeframe'] ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Budget
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <?= @$_POST['budget'] ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Message
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px; line-height:18px;">
                                                      <?= $_POST['message'] ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">                       Page URL
                                                  </td>
                                                  <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                                      <a href="#" style="color:#555555; text-decoration:none"><?= "http://"."$_SERVER[HTTP_HOST]"."$_SERVER[REQUEST_URI]"; ?></a>
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>                            
                              </table>
                          </td>
                          <td width="18"></td>
                      </tr>               
                  </table>
              </td>
          </tr>
          <tr>
         </tr>
      </table>
      </body>
      </html>
<?php
$to = 'nicolecross1579@gmail.com';
$subject = 'Contact Us';
$body = ob_get_clean();

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'From: eTraffic Web Design<noreply@etrafficwebdesign.org>' . "\r\n";
//$headers .= 'Bcc: info@etraffic.com.au' . "\r\n";
$result = @wp_mail($to,$subject,$body,$headers);

  @session_start();
  ob_start(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>eTraffic Web Design</title>
</head>
<body topmargin="0" leftmargin="0" style="padding:0; margin:0; background-color:#f7f7f7;">
<table cellpadding="0" cellspacing="0" width="100%" align="center" style="background-color:#f7f7f7; font-family:Arial, Helvetica, sans-serif">
    <tr>
        <td valign="top">
            <table cellpadding="0" cellspacing="0" width="718" align="center" style="background:url(<?= $url ?>/images/top-bg.png); background-repeat:no-repeat; background-position:center top; margin-top:20px;">
                <tr>
                    <td width="18"></td>
                    <td width="598" style="background-color:#ffffff; padding:42px" valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td height="110" align="center" valign="top">
                                    <a href="#">

                                 <img src="<?= $url ?>/images/logo.png" alt="etraffic-web-design-logo" />

                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px; border-top:2px solid #e7e7e7;">
                                
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">Dear <?= $_POST['fullname'] ?>,</td>
                           </tr>
                            <tr>                       
                                <td style="color:#000000; font-size:14px; padding:15px 0px;">Thank you for submitting your details.</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-size:14px;">We have received it and one of our professionals will contact you as soon as possible. Have a great day.</td>
                            </tr>
                             <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-size:14px;">Regards,</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">eTraffic Web Design</td>                            </tr>
                        </table>
                    </td>
                    <td width="18"></td>
                </tr>               
            </table>
        </td>
    </tr>   
</table>
</body>
</html>
<?php
    $to2 =$_POST['email'];
    $subject2 = 'Thanks for contact us';
    $body2 = ob_get_clean();
    // To send HTML mail, the Content-type header must be set
    $headers2  = 'MIME-Version: 1.0' . "\r\n";
    $headers2 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Additional headers
    $headers2 .= 'From: eTraffic Web Design<noreply@etrafficwebdesign.org>' . "\r\n";
    

    $res2=mail($to2,$subject2,$body2,$headers2);
    unset($_POST);
	$url=get_site_url()."/thank-you";
	wp_redirect($url); exit;
?>
<?php     
  }
  else
  {
    ?>
    <script type="text/javascript">
    alert("Captcha Code Incorrect !");
    </script>
<?php
  }
}
$char = strtoupper(substr(str_shuffle('abcdefghjkmnpqrstuvwxyz'), 0, 4));
$str = rand(1, 7) . rand(1, 7) . $char;
$_SESSION['captcha_id'] = $str;
get_header(); ?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
  function initialize() {

  // Create an array of styles.
  var styles = [
    {
      stylers: [
        { saturation: -100 }
      ]
    }
  ];
  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});

  // Create a map object, and include the MapTypeId to add
  // to the map type control.
  var mapOptions = {
    zoom: 14,
    center: new google.maps.LatLng(-37.8260803, 144.9690922),
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }
  };
 

  var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);
  
  var image = "<?php echo get_template_directory_uri(); ?>/images/map-logo.png";
  var myLatLng = new google.maps.LatLng(-37.8168803, 144.9570922);
  var beachMarker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image
  });


var image1 = "<?php echo get_template_directory_uri(); ?>/images/map-logo.png";
  var myLatLng1 = new google.maps.LatLng(-37.850184, 144.991623);
  var beachMarker1 = new google.maps.Marker({
      position: myLatLng1,
      map: map,
      icon: image1
  });


  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<section class="header-wrap-inner our-histry-banner" id="scroll" style="height:838px;">
<div class="contact-map" id="map-canvas" style="width: 100%; height: 100%"></div>
  <div class="header-pattern" id="header-pattern">
    <?php echo get_template_part("menu"); ?>
      	<div class="new-contact">
		<?php the_field("contact-detail"); ?>
    	</div>   
  </div>
</section>

<section class="lets-work-together-wrap">
  <div class="lets-work-together">
      <h2>&#8226; Let’s Work Together &#8226;</h2>
      <div class="contact-form">
         <form name="cform" id="cform" method="post" class="cform">
          <p>
              <label>name</label>
            <input name="fullname" type="text" class="required" value="<?= @htmlentities($_POST['fullname']) ?>">
            </p>
            
          <p>
              <label>email</label>
                <input name="email" id="email" type="text" class="required email" value="<?= @htmlentities($_POST['email']) ?>">
            </p>
            <div class="c"></div>            
          <p>
              <label><span>optional</span>Phone</label>
              <input name="phone" id="phone" type="text" class="NumbersOnly" value="<?= @htmlentities($_POST['phone']) ?>">
            </p>
            
          <div class="multi-select">
              <label>state</label>
              <div class="selectdiv">
              <select name="state"  class="selectboxdiv">
                <option selected value="">Select State</option>
                <option value="ACT">Australian Capital Territory</option>
		<option value="NSW">New South Wales</option>
		<option value="NT ">Northern Territory</option>
		<option value="QLD">Queensland</option>
		<option value="SA ">South Australia</option>
		<option value="TAS">Tasmania</option>
		<option value="VIC">Victoria</option>
		<option value="WA ">Western Australia</option>
		<option value="NFK">Norfolk Island</option>
		<option value="HRD">Heard and McDonald Islands</option>          
              </select>
              <div class="out">&nbsp;</div>
              </div>
          </div>
          
            <div class="c"></div>

<div class="multi-select" style="z-index:1000 !important;">
              <label><span>optional</span>Project requirement</label>
                <div class="c"></div>
      <div id="" class="wrapper-dropdown-4 dd"><div class="wrapper-dropdown-line"></div>Choose
        <ul class="dropdown">
          <li>
            <input type="checkbox" id="el-1" name="project[]" value="New website for a new company">
            <label for="el-1">New website for a new company</label>
          </li>
          <li>
            <input type="checkbox" id="el-2" name="project[]" value="New website for an existing company">
            <label for="el-2">New website for an existing company</label>
          </li>
          <li>
            <input type="checkbox" id="el-3" name="project[]" value="Revamping an existing website">
            <label for="el-3">Revamping an existing website</label>
          </li>
          <li>
            <input type="checkbox" id="el-4" name="project[]" value="Other">
            <label for="el-4">Other</label>
          </li>
        </ul>
      </div>
</div>
 
          <div class="multi-select" style="z-index:1200 !important">
              <label><span>optional</span>Website type</label>
                <div class="c"></div>
      <div id="" class="wrapper-dropdown-4 dd"><div class="wrapper-dropdown-line"></div>Choose
        <ul class="dropdown">
          <li>
            <input type="checkbox" id="el-5" name="website[]" value="A straightforward 'brochure style' website">
            <label for="el-5">A straightforward 'brochure style' website</label>
          </li>
          <li>
            <input type="checkbox" id="el-6" name="website[]" value="A site with Content Management System">
            <label for="el-6">A site with Content Management System</label>
          </li>
          <li>
            <input type="checkbox" id="el-7" name="website[]" value="An eCommerce website (i.e. online shop)">
            <label for="el-7">An eCommerce website (i.e. online shop)</label>
          </li>
          <li>
            <input type="checkbox" id="el-8" name="website[]" value="A Mobile Website">
            <label for="el-8">A Mobile Website</label>
          </li>
          <li>
            <input type="checkbox" id="el-9" name="website[]" value="A Web Application">
            <label for="el-9">A Web Application</label>
          </li>          
          
        </ul>
      </div>
</div>
            <div class="c"></div>
            
  <div class="c"></div>            
           
          <div class="multi-select">
              <label><span>optional</span>Time Frame</label>
              <div class="selectdiv">
              <select name="timeframe"  class="selectboxdiv">
                <option selected value="">Choose</option>
                <option  value="What is the timeframe for the website to go live?">What is the timeframe for the website to go live?</option>
                <option  value="As soon as possible">As soon as possible</option>
                <option  value="Within 1 month">Within 1 month</option>
                <option  value="Within 2 months">Within 2 months</option>
                <option  value="Within 3 months">Within 3 months</option>
                <option  value="Longer">Longer</option>          
              </select>
              <div class="out">&nbsp;</div>
              </div>
          </div>
                     
          <div class="multi-select">
              <label><span>optional</span>Budget</label>
              <div class="selectdiv">
              <select name="budget"  class="selectboxdiv">
                <option selected value="">Choose</option>
                <option  value="<$1,000.00"><$1,000.00</option>
                <option  value="$1.000 to $3,000">$1.000 to $3,000</option>
                <option  value="$3,000 to $5,000">$3,000 to $5,000</option>
                <option  value="$5,000 to $15,000">$5,000 to $15,000</option>
                <option  value="$15,000 to $30,000">$15,000 to $30,000</option>
                <option  value="$30,000 and Above">$30,000 and Above</option>              
              </select>
              <div class="out">&nbsp;</div>
              </div>
          </div>
          
        <p class="contact-textarea">
          <label>Message</label>
            <textarea name="message" cols="" rows="" value="<?= @htmlentities($_POST['message']) ?>"></textarea>
        </p>
            <div class="c"></div>
            <div class="contact-captcha">
            <img src="<?php echo get_template_directory_uri(); ?>/captcha/image.php?<?php echo time(); ?>" alt="Captcha image"  width="129" height="50" ><input type="text" placeholder="Enter captcha code here" class="required captchacode" name="captcha" id="captcha" />
            <input type="submit" class="submit"name="submit" value="I am done, send!">
            <div class="c"></div>
          </div>
           </form>
      
      </div>
    </div>
</section>

<div class="career-link-contact">
<div class="career-link">
<?php while(have_posts()) : the_post();?>
  <?php the_content(); ?>
<?php endwhile;  wp_reset_query();?>
</div>
</div>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
