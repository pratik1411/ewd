<?php
/**
 * Template Name: AboutUs Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner team-banner" id="scroll">
  <div class="header-pattern">
    <div class="banner-text-inner">
      <?php while(have_posts()) : the_post();?>
        <?php the_field('slogun'); ?>
      <?php endwhile;  wp_reset_query();?>
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-works-wrap">
  <div class="our-works">
    <div id="container">
      <div class="work-nav">
        <ul class="our-team-nav">
          <li><a href="<?php echo get_site_url(); ?>/about-us/" class="active">About Us</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-team/" >Our Team</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-history/" >Our History</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-process/">Our Process</a></li>
          <li><a href="<?php echo get_site_url(); ?>/career/">Careers</a></li>
        </ul>
        <div class="c"></div>
      </div>
      
      <div class="about">
    <div class="about-welcome">
        <?php while(have_posts()) : the_post();?>
        <?php the_content(); ?>
       <?php endwhile;  wp_reset_query();?>
        <div class="c"></div>
    </div>
    
    
<div class="etraffic-headquaters">
<div class="our-fresh-work">
  <h2>&#8226; etraffic Headquarters &#8226;</h2>
</div>
    Our modern headquarters, shown below, houses our Melbourne based design, development,<br>
and sales team. We make use of cutting edge technology and software to bring you the <br>
most advanced internet marketing solutions on the market.

</div>
  

<div class="etraffic-headquaters-slider">
  <ul class="etraffic-headquaters-slide"
        data-cycle-slides="> li"
        data-cycle-manual-fx="scrollHorz"
        data-cycle-prev=".etraffic-headquaters-slider-prev"
        data-cycle-next=".etraffic-headquaters-slider-next"
    data-cycle-pager=".banner-pager"
        >
       <?php
            $image_gallery = get_post_meta( $post->ID, '_easy_image_gallery', true );
            $attachments = array_filter( explode( ',', $image_gallery ) );
              if ( $attachments )
                foreach ( $attachments as $attachment_id ) {
          ?>
        <li><?php $image_attributes = wp_get_attachment_image_src( $attachment_id  ); ?>
	<img src="<?php echo wp_get_attachment_url($attachment_id); ?>" />
            <?php //echo wp_get_attachment_image( $attachment_id ); ?>  
        </li>
        <?php } ?>
        </ul>
    
    <div class="etraffic-headquaters-slider-prev"></div>
    <div class="etraffic-headquaters-slider-next"></div>
    
</div>
</div>

</div>
</div>
  
<div class="reason-to-choose-us">
  <div class="our-fresh-work">
        <h2>&#8226; What Our Clients Say &#8226;</h2>
  </div>
  <div class="reason-to-choose-us-sub-title">Click Play To Watch</div>
    <div class="reason-to-choose-us-video">
      <a href="#clientsay" class="clientsay-popup">
        <img src="<?php echo get_template_directory_uri(); ?>/images/index-video2.jpg">
      </a>
    </div>
</div>

<div id="clientsay" class="clientsay">
<div class="banner-video-close"><a href="#"></a></div>
<div id="wistia_otyn0yh46y" class="wistia_embed"><div itemprop="video" itemscope itemtype="http://schema.org/VideoObject"><meta itemprop="name" content="eTraffic Group - Customer Testimonial Montage" /><meta itemprop="duration" content="PT1M45S" /><meta itemprop="thumbnailUrl" content="https://embed-ssl.wistia.com/deliveries/105574245b3c4d00e7aa150e61dd0e376c6…" /><meta itemprop="contentURL" content="https://embed-ssl.wistia.com/deliveries/266ae1322c1c03b696c670b0ac38cdf4db8…" /><meta itemprop="embedURL" content="
https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04&banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin" /><meta itemprop="uploadDate" content="2013-12-16T12:53:40Z" /><object id="wistia_otyn0yh46y_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;height:360px;position:relative;width:640px;"><param name="movie" value="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04"><… name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="bgcolor" value="#000000"></param><param name="wmode" value="opaque"></param><param name="flashvars" value="banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin"></param><embed src="https://embed-ssl.wistia.com/flash/embed_player_v2.0.swf?2013-10-04" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="banner=true&controlsVisibleOnLoad=true&customColor=7b796a&fullscreenDisabled=true&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Bsize%5D=34754589&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2Fc757a07fcd68745c0f411e0e79f2d6ff8dfa77e0.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=105.0&showVolume=true&stillUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F105574245b3c4d00e7aa150e61dd0e376c60c44d.jpg%3Fimage_crop_resized%3D640x360&unbufferedSeek=false&videoUrl=https%3A%2F%2Fembed-ssl.wistia.com%2Fdeliveries%2F266ae1322c1c03b696c670b0ac38cdf4db8f9361.bin" name="wistia_otyn0yh46y_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed></object><noscript itemprop="description">eTraffic Group - Customer Testimonial Montage</noscript></div></div>
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
<script>
wistiaEmbed = Wistia.embed("otyn0yh46y");
</script>
<script charset="ISO-8859-1" src="//fast.wistia.com/embed/medias/otyn0yh46y/metadata.js"></script>
</div>
    
<div class="works-testimonials">
<?php query_posts("post_type=testimonial&orderby=rand&posts_per_page=-1"); ?>
<ul class="testi-slide"
        data-cycle-slides="> li"
        data-cycle-manual-fx="fade"
        data-cycle-prev=".testi-slide-prev"
        data-cycle-next=".testi-slide-next"
		data-cycle-pager=".portfolio-slider-nav"
		data-cycle-auto-height=container
        >
<?php while(have_posts()):the_post(); ?>
    <li><?php the_content(); ?>
    <b><?php the_title(); ?></b></li>
  <?php endwhile; ?>
</ul>
    <div class="portfolio-slider-nav"></div>


</div>

</div>
</section>
<script type="text/javascript">
    $(window).ready(function(){
        $(".banner-video-close").click(function(){
            $.fancybox.close();
        });
    });
</script>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
