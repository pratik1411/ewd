<?php
/**
 * Template Name: Thankyou Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
  <div class="header-pattern">
    <div class="banner-text-inner">
      <?php while(have_posts()) : the_post();?>
        <?php the_field('slogun'); ?>
      <?php endwhile;  wp_reset_query();?>
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-works-wrap">
  <div class="our-works">
          
      <div class="about">
    
    
  <div class="reason-to-choose-us">
<div class="our-fresh-work">
      <h2>&#8226; What Our Clients Say &#8226;</h2>
</div>
      <div class="reason-to-choose-us-sub-title">Click Play To Hear What They Have To Say</div>
        <div class="reason-to-choose-us-video">
        <a href="http://www.youtube.com/embed/IgDyQNX_-qQ?rel=0&autoplay=1" class="various iframe"><img src="<?php echo get_template_directory_uri(); ?>/images/index-video2.jpg"></a></div>
    </div>
    
<div class="works-testimonials">
<?php query_posts("post_type=testimonial&orderby=rand&posts_per_page=-1"); ?>
<ul class="testi-slide"
        data-cycle-slides="> li"
        data-cycle-manual-fx="fade"
        data-cycle-prev=".testi-slide-prev"
        data-cycle-next=".testi-slide-next"
		data-cycle-pager=".portfolio-slider-nav"
		data-cycle-auto-height=container
        >
<?php while(have_posts()):the_post(); ?>
    <li><?php the_content(); ?>
    <b><?php the_title(); ?></b></li>
  <?php endwhile; ?>
</ul>
    <div class="portfolio-slider-nav"></div>


</div>

</div>
</section>

<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
