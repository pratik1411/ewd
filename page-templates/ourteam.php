<?php
/**
 * Template Name: our team Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner team-banner" id="scroll">
  <div class="header-pattern">
    <div class="banner-text-inner">
      <?php while(have_posts()) : the_post();?>
        <?php the_content(); ?>
      <?php endwhile;  wp_reset_query();?>
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-works-wrap">
  <div class="our-works">
    <div id="container">
      <div class="work-nav">
        <ul class="our-team-nav">
          <li><a href="<?php echo get_site_url(); ?>/about-us/" >About Us</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-team/" class="active" >Our Team</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-history/" >Our History</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-process/">Our Process</a></li>
          <li><a href="<?php echo get_site_url(); ?>/career/">Careers</a></li>
            </ul>
        <div class="c"></div>
      </div>
      
      <div class="our-team">
      <?php $featureid=array(); ?>
        <?php query_posts("post_type=team"); ?>
          <ul class="director">
            <?php while(have_posts()):the_post(); ?>
              <?php if(get_field("featured")=="yes"): ?>
                <?php $featureid[]=get_the_id(); ?> 
                <li><div class="our-team-photo">
		<?php the_post_thumbnail("full"); ?>
		<img src="<?php the_field(carttonimg); ?>" class="team-hover" />
		<div class="team-social">
		
            	<a href="<?php echo get_field('gplus'); ?>" class="team-google" target="_blank"></a>
            	<a href="<?php echo get_field('twitter'); ?>" class="team-twitter" target="_blank"></a>
             	<a href="<?php echo get_field('linkedin'); ?>" class="team-linkedin" target="_blank"></a>
            	<a href="mailto:<?php echo get_field('email'); ?>" class="team-mail" target="_blank"></a> 
		
            </div></div><h4><?php the_title(); ?></h4><span><?php the_excerpt(); ?></span></li>
              <?php endif; ?>
            <?php endwhile; wp_reset_query(); ?>
          </ul>
          <div class="c"></div>  
          
          <?php $loop1 = new WP_Query(array('post_type'=>'team','post__not_in' => $featureid ) ); ?>
          <ul>
            <?php while($loop1->have_posts()):$loop1->the_post(); ?>
              <li><div class="our-team-photo"><?php the_post_thumbnail("full"); ?><div><h5>View Full Bio</h5></div></div><h4><?php the_title(); ?></h4>
              <span><?php the_excerpt(); ?></span></li>
            <?php endwhile; wp_reset_query(); ?>
          </ul>
          <div class="c"></div>
        </div>
      </div>
    </div>
    
    <div class="career-link">
      <?php //the_field("career"); ?>
      <p><span>eTraffic wants you</span>Our doors are always open for creative professionals to join our ever expanding company</p>
      <div class="c"></div>
    <div class="what-we-do-link"><a href="<?php echo get_site_url(); ?>/career/">See Recent Job Posting</a></div>
    </div>
  </div>
</section>

<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>