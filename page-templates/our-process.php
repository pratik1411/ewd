<?php
/**
 * Template Name: Our process Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="header-wrap-inner team-banner" id="scroll">
    <div class="header-pattern">
      <div class="banner-text-inner">
      <?php while(have_posts()) : the_post();?>
        <?php the_content(); ?>
      <?php endwhile;  wp_reset_query();?>
      </div>
      <?php echo get_template_part("menu"); ?>
    </div>
</section>
    
  <div class="our-works">
    <div class="work">
    <div id="container">
  <div class="work-nav">
  <ul class="our-team-nav">
   <li><a href="<?php echo get_site_url(); ?>/about-us/" >About Us</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-team/" >Our Team</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-history/" >Our History</a></li>
          <li><a href="<?php echo get_site_url(); ?>/our-process/" class="active" >Our Process</a></li>
          <li><a href="<?php echo get_site_url(); ?>/career/">Careers</a></li>
      </ul>
      <div class="c"></div>
</div>

  <div class="our-process">
  <div class="view-progress"><a href="#our-process-tab"></a></div>
      <div class="our-process-chart">
          <div class="process-chart-round1">
                        <div class="process-icon1"><a href="javascript:void(0)" id="chart1"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon1.png"></a></div>
                        <div class="process-icon2"><a href="javascript:void(0)" id="chart2"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon2.png"></a></div>
                        <div class="process-icon3"><a href="javascript:void(0)" id="chart3"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon3.png"></a></div>
                        <div class="process-icon4"><a href="javascript:void(0)" id="chart4"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon4.png"></a></div>
                        <div class="process-icon5"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon5.png"></div>
              <div class="process-chart-round2">
                    <div class="process-chart-round2-content">
              <div class="process-icon6"><a href="javascript:void(0)" id="chart5"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon6.png"></a></div>
              <div class="process-icon7"><a href="javascript:void(0)" id="chart6"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon7.png"></a></div>
              <div class="process-icon8"><a href="javascript:void(0)" id="chart7"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon8.png"></a></div>
              <div class="process-icon9"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon9.png"></div>
                        </div>
                
              <div class="process-chart-round3">
                    <div class="process-chart-round3-content">
                          <div class="process-icon10"><a href="javascript:void(0)" id="chart8"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon10.png"></a></div>
                          <div class="process-icon11"><a href="javascript:void(0)" id="chart9"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon11.png"></a></div>
                          <div class="process-icon12"><a href="javascript:void(0)" id="chart10"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon12.png"></a></div>
                          <div class="process-icon13"><a href="javascript:void(0)" id="chart11"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon13.png"></a></div>
                          <div class="process-icon14"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon14.png"></div>
                        </div>
                <div class="process-chart-round4">
                      <div class="process-chart-round4-content">
                            <div class="process-icon15"><a href="javascript:void(0)" id="chart12"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon15.png"></a></div>
                            <div class="process-icon16"><a href="javascript:void(0)" id="chart13"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon16.png"></a></div>
                            <div class="process-icon17"><a href="javascript:void(0)" id="chart14"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon17.png"></a></div>
                            <div class="process-icon18"><a href="javascript:void(0)" id="chart15"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon18.png"></a></div>
                            <div class="process-icon19"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon19.png"></div>
                            </div>
                        
                  <div class="process-chart-round5">
                        <div class="process-chart-round5-content">
                              <div class="process-icon20"><div class="blink"><a href="javascript:void(0)" id="chart16"><img src="<?php echo get_template_directory_uri(); ?>/images/process-icon20.png"></a></div></div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    
  </div>
<div class="our-process-tab" id="our-process-tab">
   <div class="process-tab">
      <?php query_posts("post_type=process&posts_per_page=-1&order=DESC"); ?>
            <ul>
            <?php $c='a'; ?>
            <?php while(have_posts()):the_post(); ?>
              <li><a href="javascript:void(0)" id="<?php echo $c; ?>"><?php the_title(); ?></a></li>
              <?php $c++; ?>
            <?php endwhile; ?>
            </ul>
     </div>
     
<div class="process-tab-disc">
<?php query_posts("post_type=process&posts_per_page=-1&orderby=ID"); ?>
<?php $a='a'; $i=1; ?>
 <?php while(have_posts()):the_post(); ?>
     <div class="<?php echo $a; ?> chart<?php echo $i; ?>">
        <?php the_content(); ?>
     <?php $a++; $i++; ?> 
     </div>
  <?php endwhile; ?>
</div>
<div class="c"></div>
 </div>

</div>
</div>
<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>