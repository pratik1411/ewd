<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner" id="scroll">
    <div class="header-pattern">
    	<?php echo get_template_part("menu"); ?>
    	<div class="banner-text-inner">
          <?php
      			$my_id = 17;
      			$post_id_5369 = get_post($my_id);
      			$content = $post_id_5369->post_content;
      			$content = apply_filters('the_content', $content);
      			$content = str_replace(']]>', ']]>', $content);
      			echo $content;
          ?>
    	</div>
	</div>
</section>
<section class="our-works-wrap">
  <div class="our-works">
      <div class="work">
    <div id="container">
  <div class="work-nav">
  <div class="blog-nav-wrap">
  <div class="blog-nav">
    <ul class="our-team-nav">
       <li class="active"><a href="<?php echo get_site_url(); ?>/blog" class="active">Blog</a></li>
      <!-- <li><a href="<?php echo get_site_url(); ?>/etraffic-tv" >eTraffic TV</a></li> -->
    </ul>
  </div>
  <div class="blog-search">
      <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<input type="hidden" name="post_type" value="post"/>
          <input type="text" value="" name="s" id="s" placeholder="Search Blog Posts"/>
          <input type="submit" id="searchsubmit" value=" " />
    </form>
  </div>
      <div class="c"></div>
  </div>
</div>
  <div class="blog-new">
    <div class="blog-new-left">
      <div class="blog-new-list-detail">
        <div>
          <div class="blog-new-author">
  <?php while(have_posts()):the_post(); ?>
<div class="blog-new-author-photo"><?php the_author_image(); ?></div>
<div class="blog-new-author-name"><?php the_author(); ?><span><?php the_time("F d,Y"); ?></span></div>
</div>
<div class="blog-new-post">
<h3><?php the_title(); ?></h3>
<div class="blog-new-category">Category: <span><?php the_category(' '); ?></span></div>
</div>
<div class="c"></div>
</div>
<div class="blog-detail-disc ">    
  <?php the_content(); ?>
</div>
</div>
<?php endwhile; ?>
<div class="blog-new-detail-author">
  <div class="blog-new-detail-author-left">
    <img src="<?php echo get_template_directory_uri(); ?>/images/cam-hover.jpg" />
    <div class="blog-new-detail-author-social-icon">
      <a href="#" class="blog-new-detail-author-social-facebook"></a>
      <a href="#" class="blog-new-detail-author-social-twitter"></a>
      <a href="#" class="blog-new-detail-author-social-google"></a>
      <a href="#" class="blog-new-detail-author-social-linkedin"></a>
    </div>
  </div>
  <div class="blog-new-detail-author-right">
              <span>Author: <strong>Cameron Francis</strong> </span>
                Cameron Francis is the Director of The eTraffic Group. With over 8 years experience within the Sales & Online Marketing field he writes about various topics to help business owners increase their online footprint and succeed online...
            </div>
            <div class="c"></div>
        </div>
<div class="blog-new-comment">
  <?php comments_template(); ?>
   
</div>
</div>
 

  <?php echo get_template_part("join_community"); ?>

<section class="from-our-journal-wrap">
<?php echo get_template_part("social_network");
	echo get_template_part("other_link");
	echo get_template_part("work_center");?>
<?php get_footer(); ?>
