<div class="social-networks">
	<h2>&#8226; Social Networks &#8226;</h2>
    <div class="twitter-box">
    	<div class="social-networks-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter-icon.png"></div>

<div class="twitter-box-bg">
	<div class="etraffic-tweet"><img src="<?php echo get_template_directory_uri(); ?>/images/twiiter-etraffic.png" /></div>
      <?php dynamic_sidebar("sidebar-2"); ?>
          <div class="follow-us-on-twitter"><a href="https://twitter.com/eTrafficGroup">Follow us on Twitter</a></div>
</div>
    </div>
    <div class="facebook-box">
    	<div class="social-networks-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook-icon.png"></div>
    <div class="facebook-box-inner">
    <div id="fb-root">
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="fb-like-box" data-href="http://www.facebook.com/eTrafficGroup" data-width="450" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
</div>
</div>
</div>
<div class="c"></div>
