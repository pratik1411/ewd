<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="header-wrap-inner work-detail-banner" id="scroll">
	<img src="<?php the_field('portfolio_banner_image');?>">
  <div class="header-pattern">
    <div class="banner-text-inner">
      <?php 
        $value = get_post_meta('15','slogun',true);
        if( ! empty( $value ) ) 
          //echo($value);
      ?>
		
<?php the_field('portfolio_header_title');?>
    </div>
    <?php echo get_template_part("menu"); ?>
  </div>
</section>

<section class="our-work-detail-wrap">
  <div class="our-work-detail">
    <div class="our-work-detail-brief">
      <?php while(have_posts()):the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; ?>
      <div class="c"></div>
        <div class="work-detail-view-share">
          <div class="work-detail-view-site"><a href="<?php the_field('siteurl'); ?>" rel="nofollow" target="_blank">View website <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-white.png"></a></div>
          <div class="work-detail-view-share-social">
            <span>Share</span>
              <?php echo do_shortcode('[ssba]'); ?>
          </div>
          <div class="c"></div>
        </div>
    </div>
    <div class="work-detail-text">
      <?php the_field("challenge"); ?>
    <div class="c"></div>
    </div>
  </div>    
</section>

<section class="work-detail-portfolio-slider-wrap">
<div class="portfolio-slider">

<div id="slideshow-2">
    <div id="cycle-2" class="cycle-slideshow portfolio-slide"
        data-cycle-slides="> div"
        data-cycle-timeout="2500"
        data-cycle-caption-template="Slide {{slideNum}} of {{slideCount}}"
        data-cycle-fx="carousel"
        data-cycle-carousel-visible="3"
        data-cycle-carousel-fluid=true
        data-allow-wrap="true"
        data-cycle-prev=".portfolio-slider-slider-prev"
        data-cycle-next=".portfolio-slider-slider-next"
        data-cycle-pager=".portfolio-slider-nav"
    data-cycle-carousel-vertical=false
        >
        <?php
            $image_gallery = get_post_meta( $post->ID, '_easy_image_gallery', true );
            $attachments = array_filter( explode( ',', $image_gallery ) );
            $i=1;
              if ( $attachments )
                foreach ( $attachments as $attachment_id ) {
          ?>
        <div data-id="<?php echo $i; ?>"><?php $image_attributes = wp_get_attachment_image_src( $attachment_id,'full'  ); ?>
            <?php echo wp_get_attachment_image( $attachment_id, 'full' ); ?>  
        </div>
        <?php } ?>
</div>
<div class="portfolio-slider-nav"></div>  
</div>
</section>


<div class="works-testimonials">
	<?php the_field("portfolio testimonial"); ?>
	<?php $des=get_field("portfolio_designation"); if($des!="") { ?>
    	<span><?php the_field("portfolio_designation"); ?></span>
    <?php } ?>
    <div class="work-detail-view-more-projects">
    	<a href="<?php echo get_site_url(); ?>/our-work/">View more projects</a>
    </div>
</div>

<?php echo get_template_part("footer_link"); ?>
<?php get_footer(); ?>
