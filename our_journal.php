<section class="from-our-journal-wrap">
<div class="from-our-journal">
    	<h2>&#8226; From our journal &#8226;</h2>
        <ul>
            <?php query_posts("post_type=post&posts_per_page=2"); ?>
            <?php while(have_posts()):the_post(); ?>
        	<li>
            	<div class="from-our-journal-by">
                	<div class="from-our-journal-by-photo"><?php the_author_image(); ?></div>
                    <span><?php the_author(); ?></span><?php the_time("M d, Y"); ?>
                </div>
                
                <div class="from-our-journal-disc">
                	<h3><?php the_title(); ?></h3>
                    <?php echo get_excerpt('200'); ?>
                    <div class="from-our-journal-read-more">
                    	<a href="<?php the_permalink(); ?>" class="from-our-journal-read-more1">Read more</a>   |   <a href="<?php the_permalink(); ?>#respond"><?php comments_number('No comment','1 comment','% comments'); ?></a>
                    </div>
                </div>
                <div class="c"></div>
            </li>
            <?php endwhile; wp_reset_query(); ?>
        </ul>
        <div class="c"></div>
    </div>